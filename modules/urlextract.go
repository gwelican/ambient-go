package modules

import (
	"html"
	"strings"

	_ "log"
	"io/ioutil"
	"net/http"

	"fmt"
	"github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
)

type UrlExtract struct {
	Valid bool
	Url   string
}

type UrlExtractInterface interface {
	ExtractUrl(text string) UrlExtract
	GetBody(url string) ([]byte, error)
	UrlTitle(text string)
}

func (o UrlExtract) String() string {
	return fmt.Sprintf("url: %s, valid: %s", o.Url, o.Valid)
}

var (
	titleExtractRegex = pcre.MustCompile(`<title>\n*?(.*?)\n*?</title>`, pcre.CASELESS)
	urlExtractRegex = pcre.MustCompile(`(http:\/\/([-a-z0-9._~%]+(:[-a-z0-9._~%]*)?@)?([_\pL\pN\pS][-_\pL\pN\pS]*(\.[-_\pL\pN\pS]+)*|([_\pL\pN\pS][-_\pL\pN\pS]*(\.[-_\pL\pN\pS]+)*\.[\pL][-\pL\pN]*[\pL]|[0-9]{1,3}(\.[0-9]{1,3}){3}|\[((([0-9a-f]{0,4})(:([0-9a-f]{0,4})){7})|(([0-9a-f]{0,4})(:([0-9a-f]{0,4}))*:(:([0-9a-f]{0,4}))+))\]))((:[1-9][0-9]{0,4}))?(\/((\([^() \t]*\))|([^() \t]*))*(?<![.,?!\]]))?)|(https:\/\/([-a-z0-9._~%]+(:[-a-z0-9._~%]*)?@)?([_\pL\pN\pS][-_\pL\pN\pS]*(\.[-_\pL\pN\pS]+)*|([_\pL\pN\pS][-_\pL\pN\pS]*(\.[-_\pL\pN\pS]+)*\.[\pL][-\pL\pN]*[\pL]|[0-9]{1,3}(\.[0-9]{1,3}){3}|\[((([0-9a-f]{0,4})(:([0-9a-f]{0,4})){7})|(([0-9a-f]{0,4})(:([0-9a-f]{0,4}))*:(:([0-9a-f]{0,4}))+))\]))((:[1-9][0-9]{0,4}))?(\/((\([^() \t]*\))|([^() \t]*))*(?<![.,?!\]]))?)|(([_\pL\pN\pS][-_\pL\pN\pS]*(\.[-_\pL\pN\pS]+)*\.[\pL][-\pL\pN]*[\pL]|[0-9]{1,3}(\.[0-9]{1,3}){3}|\[((([0-9a-f]{0,4})(:([0-9a-f]{0,4})){7})|(([0-9a-f]{0,4})(:([0-9a-f]{0,4}))*:(:([0-9a-f]{0,4}))+))\])((:[1-9][0-9]{0,4}))?\/(((\([^() \t]*\))|([^() \t]*))*(?<![.,?!\]]))?)`, pcre.CASELESS)
)

type UrlExtractModule struct {
}


func (module UrlExtractModule) ExtractURL(text string) UrlExtract {

	extract := UrlExtract{Url:"", Valid:false}

	m := urlExtractRegex.MatcherString(text, 0)

	if m.Matches() {
		//if len(result) > 0 {
		extract.Valid = true
		url := m.GroupString(0)
		if strings.HasPrefix(url, "http") {
			extract.Url = url
		} else {
			extract.Url = "http://" + url
		}
	}

	return extract

}

func (module UrlExtractModule) GetBody(url string) ([]byte, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return ioutil.ReadAll(res.Body)
}

func (module UrlExtractModule) UrlTitle(URL string) (string, error) {

	if URL == "" {
		return "", nil
	}

	body, err := module.GetBody(URL)
	if (err != nil) {
		return "", err
	}

	m := titleExtractRegex.MatcherString(string(body), 0)
	//title := titleExtractRegex.FindString(string(body))

	if m.Matches() {
		title := m.GroupString(0)
		title = strings.Replace(title, "\n", "", -1)
		title = title[strings.Index(title, ">") + 1 : strings.LastIndex(title, "<")]

		return html.UnescapeString(title), nil
	}
	return "", nil

}

