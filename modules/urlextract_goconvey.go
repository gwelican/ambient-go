package modules

import (
	_ "fmt"
	//"github.com/go-chat-bot/bot"
	_ "net/http"
	_ "net/http/httptest"
	"testing"

	. "github.com/smartystreets/goconvey/convey"

	"github.com/stretchr/testify/mock"
	"github.com/jarcoal/httpmock"
)

type MyMock struct {
	mock.Mock
}

func (u *MyMock) getBody(url string) string {
	args := u.Called(url)
	return args.String(0)
}

type FakeBodyProvider struct {

}

func (f FakeBodyProvider) ExtractURL(text string) UrlExtract {
	return UrlExtract{Valid:true, Url:"testurl.com"}
}

func (f FakeBodyProvider) GetBody(url string) ([]byte, error) {
	return []byte("test"), nil
}

func TestUrlTitleExtraction(t *testing.T) {
	underTest := UrlExtractModule{}

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder("GET", "http://index.hu", httpmock.NewStringResponder(200, "<html><head><title>bla</title></head></body></html>"))

	Convey("Url is passed", t, func() {
		Convey("it should get the title of it", func() {
			title, _ := underTest.UrlTitle("http://index.hu")
			So(title, ShouldEqual, "bla")

		})
	})
}

func TestUrlExtract(t *testing.T) {
	underTest := UrlExtractModule{}

	Convey("No URL is in the text", t, func() {
		Convey("It should return nil back", func() {
			So(underTest.ExtractURL("randomtext").Valid, ShouldEqual, false)
			So(underTest.ExtractURL("randomtext").Url, ShouldEqual, "")
		})
	})

	Convey("Given an URL", t, func() {
		Convey("When There is a prefix in front of the url", func() {
			So(underTest.ExtractURL("ahttp://google.com").Url, ShouldEqual, "http://google.com")
		})

		Convey("When There is a suffix in back of the url", func() {
			So(underTest.ExtractURL("http://google.com)").Url, ShouldEqual, "http://google.com")
		})

		Convey("with / at the end", func() {
			Convey("it should return the same URL", func() {
				So(underTest.ExtractURL("http://google.com/").Url, ShouldEqual, "http://google.com/")
				So(underTest.ExtractURL("https://google.com/").Url, ShouldEqual, "https://google.com/")

				So(underTest.ExtractURL("http://stackoverflow.com/").Url, ShouldEqual, "http://stackoverflow.com/")
				So(underTest.ExtractURL("https://stackoverflow.com/").Url, ShouldEqual, "https://stackoverflow.com/")

				So(underTest.ExtractURL("http://test.test-75.1474.stackoverflow.com/").Url, ShouldEqual, "http://test.test-75.1474.stackoverflow.com/")
				So(underTest.ExtractURL("https://test.test-75.1474.stackoverflow.com/").Url, ShouldEqual, "https://test.test-75.1474.stackoverflow.com/")

			})
		})
		Convey("is just an URL without extra text", func() {
			So(underTest.ExtractURL("http://google.com").Url, ShouldEqual, "http://google.com")
			So(underTest.ExtractURL("https://google.com").Url, ShouldEqual, "https://google.com")

			So(underTest.ExtractURL("http://stackoverflow.com").Url, ShouldEqual, "http://stackoverflow.com")
			So(underTest.ExtractURL("https://stackoverflow.com").Url, ShouldEqual, "https://stackoverflow.com")
		})
		Convey("there is a prefix text and some text after a space", func() {
			So(underTest.ExtractURL("www.google.com/hello/bleh blah....../").Url, ShouldEqual, "http://www.google.com/hello/bleh")
		})
		Convey("the schema is not recongized it should return a proper schema url", func() {
			So(underTest.ExtractURL("www.google.com/").Url, ShouldEqual, "http://www.google.com/")
		})
		Convey("schema is not found it should return a proper schema url", func() {
			So(underTest.ExtractURL("google.com/blah/hello.php?x=11_x.hi").Url, ShouldEqual, "http://google.com/blah/hello.php?x=11_x.hi")
			So(underTest.ExtractURL("www.example.com/etcetc").Url, ShouldEqual, "http://www.example.com/etcetc")
		})
		Convey("contains query parameter and no schema it should return proper schema url with query parameters", func() {
			So(underTest.ExtractURL("example.com/etcetc?query=aasd").Url, ShouldEqual, "http://example.com/etcetc?query=aasd")
			So(underTest.ExtractURL("example.com/etcetc?query=aasd&dest=asds").Url, ShouldEqual, "http://example.com/etcetc?query=aasd&dest=asds")
		})
		Convey("even it is long it should be properly parsed", func() {
			So(underTest.ExtractURL("http://test.test-75.1474.stackoverflow.com/").Url, ShouldEqual, "http://test.test-75.1474.stackoverflow.com/")
			So(underTest.ExtractURL("http://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www").Url, ShouldEqual, "http://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www")
			So(underTest.ExtractURL("https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www").Url, ShouldEqual, "https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www")
			So(underTest.ExtractURL("https://www.youtube.com/watch?v=0QYFqX5UKM4&index=35&list=PLhCH_nPE4JTp4skkIZZOvBN6vLL_xTN53").Url, ShouldEqual, "https://www.youtube.com/watch?v=0QYFqX5UKM4&index=35&list=PLhCH_nPE4JTp4skkIZZOvBN6vLL_xTN53")
		})
		Convey("contains username and password it should return plain url", func() {
			So(underTest.ExtractURL("user:pass@example.com/etcetc").Url, ShouldEqual, "http://example.com/etcetc")
		})
		Convey("is index.hu", func() {
			So(underTest.ExtractURL("http://index.hu").Url, ShouldEqual, "http://index.hu")
		})
	})
}
