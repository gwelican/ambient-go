package modules

import (
	"github.com/eefret/gomdb"
	"fmt"
	"log"
	"strings"
)

type ImdbInterface interface {
	GetById(id string) (string, error)
	GetByTitle(title string) (string, error)
}

type imdbClientInterface interface {
	searchById(id string) (*gomdb.MovieResult, error)
	searchByTitle(title string) (*gomdb.SearchResponse, error)
}

type ImdbModule struct {
	Client imdbClientInterface
}

type ImdbClient struct {

}

func (module ImdbClient) searchById(id string) (*gomdb.MovieResult, error) {
	return gomdb.MovieByImdbID(id)
}

func (module ImdbClient) searchByTitle(title string) (*gomdb.SearchResponse, error) {
	query := &gomdb.QueryData{Title: title, SearchType: gomdb.MovieSearch}
	return gomdb.Search(query)
}

func NewImdbModule() ImdbModule {
	return ImdbModule{Client: ImdbClient{}}
}

func (module ImdbModule) GetById(id string) (string, error) {
	res, err := module.Client.searchById(id)

	if err != nil {
		return "", err
	}

	if res != nil {
		return fmt.Sprintf("[IMDb] %s %s/10 from %s votes [%s] [%s] http://imdb.com/title/%s", res.Title, res.ImdbRating, res.ImdbVotes, res.Runtime, res.Genre, res.ImdbID), nil
	}

	return "No result found", nil
}

func (module ImdbModule) GetByTitle(title string) (string, error) {
	res, err := module.Client.searchByTitle(title)

	if err != nil {
		fmt.Println(err)
		return "", err
	}

	if (len(res.Search) == 1) {
		return module.GetById(res.Search[0].ImdbID)
	} else {
		for _, movie := range res.Search {
			if (strings.ToLower(title) == strings.ToLower(movie.Title)) {
				return module.GetById(movie.ImdbID)
			}
			log.Print(movie.Title)
		}
		log.Print(res)
		return "Too much hit", nil
	}
	return "No result found", nil
}
