package modules

import (
	"fmt"
	"time"

	"gopkg.in/pg.v4"
	_ "log"
)

var (
	db = pg.Connect(&pg.Options{
		User:     "x",
		Password: "xr",
		Addr:     "x",
		Database: "x",
	})
)

type SeenInterface interface {
	Part(user,channel string)
	Quit(user,channel string)
	LastSeen(user,channel string)
}

type SeenModule struct {

}


type Seen struct {
	Id      int
	Nick    string
	Channel string
	Time    time.Time
	Message string
}

func (module SeenModule) LastSeen(nick string, channel string) {
	s := Seen{}
	db.Model(&s).Where("nick = ?", nick).Where("channel = ? ", channel).Limit(1).Select()

	// if err != nil {
	// 	panic(err)
	// }

	fmt.Printf("%+v\n", s)
}

func (module SeenModule) Part(nick string, channel string, message string) {
	s := Seen{Nick: nick, Channel: channel, Message: message, Time: time.Now()}
	fmt.Printf("%+v\n", s)
	db.Create(&s)

	// db.Update(model)
	// db.Create(&s)
}

//func CreateSchema() {
//	userLeft("gwelican2", "#d3", "lofasz")
//	seen("gwelican2", "#d3")
//	// s := Seen{
//	// 	Id: 5,
//	// 	// Nick:    "test",
//	// 	// Channel: "#test",
//	// 	// Time:    time.Now(),
//	// 	// Message: "test",
//	// }
//	//
//	// // db.Create(&s)
//	//
//	// // var seens []Seen
//	// // db.Model(&seens).Where("Id = ?", 5).Select()
//	// // fmt.Printf("%+v\n", seens)
//	// db.Select(&s)
//	//
//	// fmt.Printf("%+v\n", s)
//	//
//}
