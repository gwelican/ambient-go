package modules

import (
	_ "github.com/eefret/gomdb"
	_ "github.com/onsi/ginkgo"
	_ "github.com/onsi/gomega"
	_ "github.com/vektra/errors"

	_ "errors"
)

//
//type ImdbClientStub struct {
//	Res *gomdb.MovieResult
//	Err error
//	SearchResponse gomdb.SearchResponse
//}
//
//func (module ImdbClientStub) searchById(id string) (*gomdb.MovieResult, error) {
//	return  module.Res, module.Err
//}
//func (module ImdbClientStub) searchByTitle(title string) (*gomdb.SearchResponse, error) {
//	return  module.SearchResponse
//	//query := &gomdb.QueryData{Title: title, SearchType: gomdb.MovieSearch}
//	return gomdb.Search(query)
//}
//
//var _ = Describe("Imdb", func() {
//
//	Context("when user asks for a specific imdbid", func() {
//		Context("and it does not exists", func() {
//			underTest := ImdbModule{Client:ImdbClientStub{Err:nil, Res:nil}}
//			It("should return no results found", func() {
//				res, err := underTest.GetById("t1")
//				gomega.Expect(res).To(gomega.Equal("No result found"))
//				gomega.Expect(err).To(gomega.BeNil())
//
//			})
//
//		})
//
//		Context("and there is an error", func() {
//			errorMessage := "random error"
//
//			underTest := ImdbModule{Client:ImdbClientStub{Err:errors.New(errorMessage), Res:nil}}
//			It("should return empty string and the error", func() {
//				res, err := underTest.GetById("t1")
//				gomega.Expect(res).To(gomega.Equal(""))
//				gomega.Expect(err).To(gomega.Equal(errors.New(errorMessage)))
//
//			})
//
//		})
//
//		Context("and it does exist", func() {
//
//			fightClub := &gomdb.MovieResult{ImdbID:"t1", Title: "fight club", ImdbRating: "8.1", ImdbVotes: "61322", Runtime: "132m", Genre: "comedy"}
//
//			underTest := ImdbModule{Client:ImdbClientStub{Err:nil, Res:fightClub}}
//
//			It("should return the correclty formatted movie details", func() {
//				res, err := underTest.GetById("t1")
//				gomega.Expect(res).To(gomega.Equal("[IMDb] fight club 8.1/10 from 61322 votes [132m] [comedy] http://imdb.com/title/t1"))
//				gomega.Expect(err).To(gomega.BeNil())
//
//			})
//
//		})
//
//	})
//	Context("user asks for a specific title", func() {
//		It("should return a movie by a specific id", func() {
//			fightClub := gomdb.SearchResult{ImdbID:"t1", Title: "fight club", Year: "2016", Type: gomdb.MovieSearch}
//			fightForYourLife := gomdb.SearchResult{ImdbID:"t2", Title: "fight for your life", Year: "2013", Type: gomdb.MovieSearch}
//
//			results := [2]gomdb.SearchResult{fightClub, fightForYourLife}
//			response := gomdb.SearchResponse{Search:results}
//
//			underTest := ImdbModule{Client:ImdbClientStub{Err:nil, Res:nil, SearchResponse:response}}
//			gomega.Expect(underTest.GetByTitle("fight")).To(gomega.Equal(""))
//		})
//
//	})
//})
