package modules

import (

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	_ "github.com/jarcoal/httpmock"
)

var _ = Describe("Urlextract", func() {

		underTest := UrlExtractModule{}

		Context("No URL is in the text", func() {
			It("It should return nil back", func() {
				Expect(underTest.ExtractURL("randomtext").Valid).To(Equal(false))
				Expect(underTest.ExtractURL("randomtext").Url).To(Equal(""))
			})
		})

		Context("Given an URL", func() {

			It("When There is a prefix in front of the url", func() {
				Expect(underTest.ExtractURL("ahttp://google.com").Url).To(Equal("http://google.com"))
			})

			It("When There is a suffix in back of the url", func() {
				Expect(underTest.ExtractURL("http://google.com)").Url).To(Equal("http://google.com"))
			})

			Context("with / at the end", func() {
				It("it should return the same URL", func() {
					Expect(underTest.ExtractURL("http://google.com/").Url).To(Equal("http://google.com/"))
					Expect(underTest.ExtractURL("https://google.com/").Url).To(Equal("https://google.com/"))

					Expect(underTest.ExtractURL("http://stackoverflow.com/").Url).To(Equal("http://stackoverflow.com/"))
					Expect(underTest.ExtractURL("https://stackoverflow.com/").Url).To(Equal("https://stackoverflow.com/"))

					Expect(underTest.ExtractURL("http://test.test-75.1474.stackoverflow.com/").Url).To(Equal("http://test.test-75.1474.stackoverflow.com/"))
					Expect(underTest.ExtractURL("https://test.test-75.1474.stackoverflow.com/").Url).To(Equal("https://test.test-75.1474.stackoverflow.com/"))

				})
			})
			It("is just an URL without extra text", func() {
				Expect(underTest.ExtractURL("http://google.com").Url).To(Equal("http://google.com"))
				Expect(underTest.ExtractURL("https://google.com").Url).To(Equal("https://google.com"))

				Expect(underTest.ExtractURL("http://stackoverflow.com").Url).To(Equal("http://stackoverflow.com"))
				Expect(underTest.ExtractURL("https://stackoverflow.com").Url).To(Equal("https://stackoverflow.com"))
			})
			It("there is a prefix text and Expect()me text after a space", func() {
				Expect(underTest.ExtractURL("www.google.com/hello/bleh blah....../").Url).To(Equal("http://www.google.com/hello/bleh"))
			})
			It("the schema is not recongized it should return a proper schema url", func() {
				Expect(underTest.ExtractURL("www.google.com/").Url).To(Equal("http://www.google.com/"))
			})
			It("schema is not found it should return a proper schema url", func() {
				Expect(underTest.ExtractURL("google.com/blah/hello.php?x=11_x.hi").Url).To(Equal("http://google.com/blah/hello.php?x=11_x.hi"))
				Expect(underTest.ExtractURL("www.example.com/etcetc").Url).To(Equal("http://www.example.com/etcetc"))
			})
			It("contains query parameter and no schema it should return proper schema url with query parameters", func() {
				Expect(underTest.ExtractURL("example.com/etcetc?query=aasd").Url).To(Equal("http://example.com/etcetc?query=aasd"))
				Expect(underTest.ExtractURL("example.com/etcetc?query=aasd&dest=asds").Url).To(Equal("http://example.com/etcetc?query=aasd&dest=asds"))
			})
			It("even it is long it should be properly parsed", func() {
				Expect(underTest.ExtractURL("http://test.test-75.1474.stackoverflow.com/").Url).To(Equal("http://test.test-75.1474.stackoverflow.com/"))
				Expect(underTest.ExtractURL("http://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www").Url).To(Equal("http://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www"))
				Expect(underTest.ExtractURL("https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www").Url).To(Equal("https://stackoverflow.com/questions/6427530/regular-expression-pattern-to-match-url-with-or-without-http-www"))
				Expect(underTest.ExtractURL("https://www.youtube.com/watch?v=0QYFqX5UKM4&index=35&list=PLhCH_nPE4JTp4skkIZZOvBN6vLL_xTN53").Url).To(Equal("https://www.youtube.com/watch?v=0QYFqX5UKM4&index=35&list=PLhCH_nPE4JTp4skkIZZOvBN6vLL_xTN53"))
			})
			It("contains username and password it should return plain url", func() {
				Expect(underTest.ExtractURL("user:pass@example.com/etcetc").Url).To(Equal("http://example.com/etcetc"))
			})

			It("is index.hu", func() {
				Expect(underTest.ExtractURL("http://index.hu").Url).To(Equal("http://index.hu"))
			})
		})
})
