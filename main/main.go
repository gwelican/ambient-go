package main

import (
	irc "github.com/fluffle/goirc/client"
	"log"
	"strings"
	"io/ioutil"
	"encoding/json"
	"../modules"
)

type database struct {
	Username string
	Password string
	Address  string
	Database string
}

type Configuration struct {
	Server   string
	User     string
	Nick     string
	Password string
	Usetls   bool
	Debug    bool
	Channels []string
	Database database
}

var (
	Config Configuration
	commandCharacter = "!"
	ActiveCommands = make(map[string]irc.HandlerFunc)
	PassiveCommands = []irc.HandlerFunc{}

	extractModule = modules.UrlExtractModule{}

	ignore  = []string {"Ambient2"}
)

func init() {

	filename := "config.json"
	file, _ := ioutil.ReadFile(filename)

	json.Unmarshal(file, &Config)

	//seen
}

func registerCommand(command string, function irc.HandlerFunc) {
	ActiveCommands[command] = function
}

func registerPassiveCommand(function irc.HandlerFunc) {
	PassiveCommands = append(PassiveCommands, function)
}

func main() {

	log.Printf("%+v", Config)

	registerCommand("seen", seenCommand)
	registerCommand("imdb", imdbCommand)
	registerCommand("curr", currCommand)
	registerPassiveCommand(url)

	ircConf := irc.NewConfig(Config.Nick)

	bot := irc.Client(ircConf)

	bot.HandleFunc(irc.CONNECTED, func(conn *irc.Conn, line *irc.Line) {
		log.Print("Connected to server")
		for _, c := range Config.Channels {
			conn.Join(c)
		}
	})

	bot.HandleFunc(irc.PRIVMSG, privmsg)

	quit := make(chan bool)

	bot.HandleFunc(irc.PART, part)
	bot.HandleFunc(irc.DISCONNECTED, func(conn *irc.Conn, line *irc.Line) {
		quit <- true
	})

	bot.Connect()
	//bot.ConnectTo(ircConf.Server)

	<-quit

}

func firstWord(text string) string {
	words := strings.Fields(text)
	if (len(words) > 0) {
		return  words[0]
	} else  {
		return  strings.TrimSpace(text)
	}
}



func privmsg(conn *irc.Conn, line *irc.Line) {

	for _, nick := range ignore {
		if strings.ToLower(nick) == strings.ToLower(line.Nick) {
			return
		}
	}

	for _, cmd := range PassiveCommands {
		cmd(conn, line)
	}
	text := line.Text()

	if (strings.HasPrefix(text, commandCharacter)) {

		commandWithoutPrefix := strings.TrimPrefix(firstWord(text), commandCharacter)

		log.Print(commandWithoutPrefix)
		if _, ok := ActiveCommands[commandWithoutPrefix]; ok {
			ActiveCommands[commandWithoutPrefix](conn, line)
		}

	}
}

func part(conn *irc.Conn, line *irc.Line) {

}

func imdbCommand(conn *irc.Conn, line *irc.Line) {
	words := strings.Fields(line.Text())

	v, _ := modules.GetByTitle(strings.Join(words[1:], " "))
	conn.Privmsg(line.Target(), v)
}

func currCommand(conn *irc.Conn, line *irc.Line) {
	conn.Privmsg(line.Target(), "curr")

}
func seenCommand(conn *irc.Conn, line *irc.Line) {
	conn.Privmsg(line.Target(), "seen")
}

func url(conn *irc.Conn, line *irc.Line) {

	text := line.Text()

	url := extractModule.ExtractURL(text)
	if (url.Valid) {
		conn.Privmsg(line.Target(), url.Url)
	}
}
